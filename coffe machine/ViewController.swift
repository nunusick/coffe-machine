//
//  ViewController.swift
//  coffe machine
//
//  Created by Admin on 11/1/18.
//  Copyright © 2018 s. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        machine.isOn = false
        onOffText.setTitle("On", for: UIControl.State(rawValue:0))
        updateMachineState()
    }
    
    @IBOutlet weak var addText: UIButton!
    @IBOutlet weak var stuffLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var wishes: [(ingredient: Ingredient, portionsCount: Double)]?
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var ingr: Ingredient?
        
        switch state {
        case .SUGAR: ingr = .sugar
        case .WATER: ingr = .water
        case .MILK: ingr = .milk
        case .BEANS: ingr = .beans
        default: ingr = nil
        }
        
        if let stuff = Double(textField.text!) {
            if let ing = ingr {
                machine.addStuff([(stuff: ing, quantity: stuff)])
                textField.text = ""
                textField.isHidden = true
                textField.resignFirstResponder()
            }
            else {
                stuffLabel.text = "Something mad happened..."
                textField.isHidden = true
                textField.resignFirstResponder()
                return false
            }
        }
        else {
            stuffLabel.text = "Please enter correct value"
            textField.isHidden = false
            textField.becomeFirstResponder()
            return false
        }
        updateMachineState()
        state.next()
        addToMachine(addText)
        return true
    }
    
    enum ings {
        case SUGAR, WATER, MILK, BEANS, enD
        mutating func next() {
            switch self {
            case .SUGAR:
                self = .WATER
            case .WATER:
                self = .MILK
            case .MILK:
                self = .BEANS
            case .BEANS:
                self = .enD
            case .enD: break
            }
        }
    }
    @IBOutlet weak var mSugarBtn: UIButton!
    @IBOutlet weak var mWaterBtn: UIButton!
    @IBOutlet weak var mMilkBtn: UIButton!
    
    @IBAction func moreSugar(_ sender: UIButton) {
        if wishes != nil && !wishes!.isEmpty {
            for (i, w) in wishes!.enumerated() {
                if w.ingredient == .sugar {
                    wishes![i] = (w.ingredient, w.portionsCount + Double(1))
                    mSugarBtn.setTitle("more sugar +\(wishes![i].portionsCount)", for: UIControl.State(rawValue:0))
                    return
                }
            }
        }
        
        wishes!.append((.sugar, Double(1)))
        mSugarBtn.setTitle("more sugar +\(1)", for: UIControl.State(rawValue:0))
    }
    
    @IBAction func moreMilk(_ sender: UIButton) {
        if wishes != nil && !wishes!.isEmpty {
            for (i, w) in wishes!.enumerated() {
                if w.ingredient == .milk {
                    wishes![i] = (w.ingredient, w.portionsCount + Double(1))
                    mMilkBtn.setTitle("more milk +\(wishes![i].portionsCount)", for: UIControl.State(rawValue:0))
                    return
                }
            }
        }
        
        wishes!.append((.milk, Double(1)))
        mMilkBtn.setTitle("more milk +\(1)", for: UIControl.State(rawValue:0))
    }
    
    @IBAction func moreWater(_ sender: UIButton) {
        if wishes != nil && !wishes!.isEmpty {
            for (i, w) in wishes!.enumerated() {
                if w.ingredient == .water {
                    wishes![i] = (w.ingredient, w.portionsCount + Double(1))
                    mWaterBtn.setTitle("more water +\(wishes![i].portionsCount)", for: UIControl.State(rawValue:0))
                    return
                }
            }
        }
        
        wishes!.append((.water, Double(1)))
        mWaterBtn.setTitle("more water +\(1)", for: UIControl.State(rawValue:0))
    }
    
    var state = ings.SUGAR
    
    @IBAction func addToMachine(_ sender: UIButton) {
        guard machine.isOn else {
            stuffLabel.text = "Turn on the machine!"
            return
        }
        
        if state != .enD {
            stuffLabel.text = "ENTER \(state) VALUE"
            textField.isHidden = false
            textField.becomeFirstResponder()
        }
        else {
            state = ings.SUGAR
            textField.resignFirstResponder()
            textField.isHidden = true
        }
    }
    
    @IBAction func doEspresso(_ sender: UIButton) {
        guard machine.isOn else {
            lblHeader.text = "I'm off. turn me on!"
            return
        }
        
        wishes = []
        machine.currentDrink = .espresso
        lblHeader.text = "We'll try to make espresso for you, honey)"
    }
 
    @IBOutlet weak var statusField: UILabel!
    
    func updateMachineState() {
        let state = machine.getCurrentState()
        var stateText = ""
        
        for (_, s) in state.enumerated() {
            stateText += "\(s.0): \(s.1) "
        }
        
        stuffLabel.text = "Stuff available in containers"
        statusField.text = stateText
    }
    
    @IBOutlet weak var onOffText: UIButton!
    
    @IBAction func doAmericano(_ sender: UIButton) {
        guard machine.isOn else {
            lblHeader.text = "I'm off. turn me on!"
            return
        }
   
        wishes = []
        machine.currentDrink = .amerikano
        lblHeader.text = "We'll try to make americano for you, honey)"
    }
    
    @IBAction func doLatte(_ sender: UIButton) {
        guard machine.isOn else {
            lblHeader.text = "I'm off. turn me on!"
            return
        }
        
        wishes = []
        machine.currentDrink = .late
        lblHeader.text = "We'll try to make latte for you, honey)"
    }
    
    @IBAction func doDoubleEspresso(_ sender: UIButton) {
        guard machine.isOn else {
            lblHeader.text = "I'm off. turn me on!"
            return
        }
        
        wishes = []
        machine.currentDrink = .doubleEspresso
        lblHeader.text = "Ok, then double espresso, right? push MAKE, motherfucken dich!"
    }
    
    @IBAction func doDoubleAmericano(_ sender: UIButton) {
        guard machine.isOn else {
            lblHeader.text = "I'm off. turn me on!"
            return
        }
        
        wishes = []
        machine.currentDrink = .doubleAmerikano
        lblHeader.text = "Fine, you chose double americano"
    }
    
    @IBAction func onOff(_ sender: UIButton) {
        if sender.currentTitle! == "On" {
            machine.isOn = true
            sender.setTitle("Off", for: UIControl.State(rawValue: 0))
        }
        else {
            machine.isOn = false
            sender.setTitle("On", for: UIControl.State(rawValue: 0))
        }
    }
    
    func makeCoffee() {
        guard machine.isOn else {
            lblHeader.text = "I'm off. turn me on!"
            return
        }
        
        guard machine.currentDrink != nil else {
            lblHeader.text = "Choose something, OMG!"
            return
        }
        
        print(wishes ?? "no wishes")
        
        let result = machine.makeDrink(machine.currentDrink!, wishes: self.wishes)
        
        updateMachineState()
        print(result.0 == true ? "success " : "fail ", result.1.joined(separator: " && "))
        lblHeader.text = (result.0 == true ? "Success " : "Fail ") + result.1.joined(separator: ", ")
    }
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBAction func pressed(_ sender: UIButton) {
        lblHeader.text = "Making, please wait."
        makeCoffee()
    }
    
    let machine = CoffeeMachine(0, 0, 0, 0)
}

