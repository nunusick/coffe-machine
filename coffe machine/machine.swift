//
//  machine.swift
//  coffe machine
//
//  Created by Admin on 11/11/18.
//  Copyright © 2018 s. All rights reserved.
//

import Foundation

enum Ingredient {
    case beans, water, milk, sugar
}

enum Drink {
    case espresso, late, amerikano, doubleEspresso, doubleAmerikano
}

class CoffeeMachine: NSObject {
    
    private let beansContainerCapacity = 700.0
    private let waterContainerCapacity = 1000.0
    private let milkContainerCapacity = 500.0
    private let sugarContainerCapacity = 800.0
    
    static let recipes: [Drink: [Ingredient : Double]] =
        [
            .espresso: [.beans: 45.0, .water: 30.0, .sugar: 10.0],
            .late: [.beans: 30.0, .water: 100.0, .sugar: 30.0, .milk: 50.0],
            .amerikano: [.beans: 45.0, .water: 60.0, .sugar: 20.0],
            .doubleEspresso: [.beans: 90.0, .water: 60.0, .sugar: 20.0],
            .doubleAmerikano: [.beans: 90.0, .water: 100.0, .sugar: 40.0]
        ]
    
    var isOn: Bool
    
    var currentDrink: Drink? = nil
    private var currentBeansQuantity: Double {
        willSet {
            prevBeansQuantity = currentBeansQuantity
        }
    }
    private var currentWaterQuantity: Double {
        willSet {
            prevWaterQuantity = currentWaterQuantity
        }
    }
    private var currentMilkQuantity: Double {
        willSet {
            prevMilkQuantity = currentMilkQuantity
        }
    }
    private var currentSugarQuantity: Double {
        willSet {
            prevSugarQuantity = currentSugarQuantity
        }
    }
    
    private var prevBeansQuantity: Double = 0
    private var prevWaterQuantity: Double = 0
    private var prevMilkQuantity: Double = 0
    private var prevSugarQuantity: Double = 0
    
    func getCurrentState() -> [(Ingredient, Double)] {
        return [
            (.beans, currentBeansQuantity), (.water, currentWaterQuantity),
            (.milk, currentMilkQuantity), (.sugar, currentSugarQuantity)
               ]
    }
    
    func restorePreviousValues(stuff: Ingredient) {
        switch stuff {
        case .beans:
            currentBeansQuantity = prevBeansQuantity
        case .water:
            currentWaterQuantity = prevWaterQuantity
        case .sugar:
            currentSugarQuantity = prevSugarQuantity
        case .milk:
            currentMilkQuantity = prevMilkQuantity
        }
    }
    
    init(_ currentBeansQuantity: Double, _ currentWaterQuantity: Double, _ currentSugarQuantity: Double, _ currentMilkQuantity: Double) {
        isOn = true
        self.currentBeansQuantity = currentBeansQuantity
        self.currentWaterQuantity = currentWaterQuantity
        self.currentSugarQuantity = currentSugarQuantity
        self.currentMilkQuantity = currentMilkQuantity
        super.init()
        self.addStuff([(stuff: .sugar, quantity: 800.00), (stuff: .milk, quantity: 300.00), (stuff: .water, quantity: 1000.00), (stuff: .beans, quantity: 500.00)])
    }
    
    func makeDrink(_ drink: Drink, wishes additions: [(ingredient: Ingredient, portionsCount: Double)]?) -> (Bool, [String]) {
        guard isOn else {
            return (false, ["Turn On The Machine!"])
        }
        
        print(drink, additions ?? "")
        currentDrink = drink
        
        var recipe = CoffeeMachine.recipes[drink]!
        
        print(recipe)
        if additions != nil && !additions!.isEmpty {
            for (_, i) in additions!.enumerated() {
                if recipe[i.ingredient] == nil {
                    recipe[i.ingredient] = i.portionsCount
                }
                else {
                    recipe[i.ingredient]! += recipe[i.ingredient]! * i.portionsCount
                }
            }
        }
        print(recipe)
        
        var result: [String] = []
        var bChanged = false, mChanged = false, sChanged = false, wChanged = false
        
        for ing in recipe.keys {
            switch ing {
            case .beans:
                if currentBeansQuantity < recipe[ing]! {
                    result.append("Not enough coffee beans in machine, need \(recipe[ing]! - currentBeansQuantity) more to make drink, please add")
                }
                else {
                    currentBeansQuantity -= recipe[ing]!
                    bChanged = true
                }
            case .milk:
                if currentMilkQuantity < recipe[ing]! {
                    result.append("Not enough milk in machine, need \(recipe[ing]! - currentMilkQuantity) more to make drink, please add")
                }
                else {
                    currentMilkQuantity -= recipe[ing]!
                    mChanged = true
                }
            case .sugar:
                if currentSugarQuantity < recipe[ing]! {
                    result.append("Not enough sugar in machine, need \(recipe[ing]! - currentSugarQuantity) more to make drink, please add")
                }
                else {
                    currentSugarQuantity -= recipe[ing]!
                    sChanged = true
                }
            case .water:
                if currentWaterQuantity < recipe[ing]! {
                    result.append("Not enough water in machine, need \(recipe[ing]! - currentWaterQuantity) more to make drink, please add")
                }
                else {
                    currentWaterQuantity -= recipe[ing]!
                    wChanged = true
                }
            }
        }
        
        if !result.isEmpty {
            if bChanged {
                restorePreviousValues(stuff: .beans)
            }
            
            if wChanged {
                restorePreviousValues(stuff: .water)
            }
            
            if sChanged {
                restorePreviousValues(stuff: .sugar)
            }
            
            if mChanged {
                restorePreviousValues(stuff: .milk)
            }
        }
        
        print("water left: \(currentWaterQuantity), sugar left: \(currentSugarQuantity), milk left: \(currentMilkQuantity), beans left: \(currentBeansQuantity)")
        return result.isEmpty ? (true, ["take your coffee, please, sir"]) : (false, result)
    }
    
    func addStuff(_ what: [(stuff: Ingredient, quantity: Double)]) {
        
        guard isOn else {
            print("Turn on the machine!")
            return
        }
        
        for (n, w) in what.enumerated() {
            print(n, w)
            switch w.stuff {
        
            case .beans:
                if currentBeansQuantity + w.quantity < 0 {
                    currentBeansQuantity = 0
                    continue
                }
                
                if currentBeansQuantity + w.quantity > beansContainerCapacity {
                    currentBeansQuantity = beansContainerCapacity
                    continue
                }
                
                currentBeansQuantity += w.quantity
            
            case .milk:
                if currentMilkQuantity + w.quantity < 0 {
                    currentMilkQuantity = 0
                    continue
                }
                
                if currentMilkQuantity + w.quantity > milkContainerCapacity {
                    currentMilkQuantity = milkContainerCapacity
                    continue
                }
                
                currentMilkQuantity += w.quantity
            
            case .sugar:
                if currentSugarQuantity + w.quantity < 0 {
                    currentSugarQuantity = 0
                    continue
                }
                
                if currentSugarQuantity + w.quantity > sugarContainerCapacity {
                    currentSugarQuantity = sugarContainerCapacity
                    continue
                }
                
                currentSugarQuantity += w.quantity
            
            case .water:
                if currentWaterQuantity + w.quantity < 0 {
                    currentWaterQuantity = 0
                    continue
                }
                
                if currentWaterQuantity + w.quantity > waterContainerCapacity {
                    currentWaterQuantity = waterContainerCapacity
                    continue
                }
                
                currentWaterQuantity += w.quantity
            }
        }
    }
}
